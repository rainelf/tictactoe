﻿using System;
using System.Windows;
using System.Windows.Shapes;

namespace TicTacToe
{

    public class Marked : DependencyObject
    {
        public static readonly DependencyProperty MarkedProperty = DependencyProperty.RegisterAttached(
            "Marked", typeof(bool), typeof(Marked), new PropertyMetadata(false));


        public static void SetIsMarked(Rectangle target, Boolean value)
        {
            target.SetValue(MarkedProperty, value);
        }

        public static bool GetIsMarked(Rectangle target)
        {
            return (bool)target.GetValue(MarkedProperty);
        }
    }

}