﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TicTacToe;
namespace TicTacToe
{
  
    public partial class MainWindow : Window
    {

        private ViewModel viewModel = null;

        public MainWindow()
        {
            InitializeComponent();        
            StartButton.Click += new RoutedEventHandler(Button_Click);
        }

        private void Pvp_Checked(object sender, RoutedEventArgs e)
        {
            ViewModel.playMode = Constants.PVP;
        }

        private void Pve_Checked(object sender, RoutedEventArgs e)
        {
            ViewModel.playMode = Constants.PVP;
        }

        private void PlayWithX_Checked(object sender, RoutedEventArgs e)
        {
            ViewModel.playerPlaysWith = Constants.PLAY_WITH_X;
        }

        private void PlayWith0_Checked(object sender, RoutedEventArgs e)
        {
            ViewModel.playerPlaysWith = Constants.PLAY_WITH_0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.viewModel = new ViewModel();
            ViewModel.gameStarted = true;
            ((Button)sender).IsEnabled = false;      
            
            ViewModel.Cell1 = Cell1;
            ViewModel.Cell2 = Cell2;
            ViewModel.Cell3 = Cell3;
            ViewModel.Cell4 = Cell4;
            ViewModel.Cell5 = Cell5;
            ViewModel.Cell6 = Cell6;
            ViewModel.Cell7 = Cell7;
            ViewModel.Cell8 = Cell8;
            ViewModel.Cell9 = Cell9;

            if (this.viewModel.isComputerTurn())
            {
                this.viewModel.computerMarksCell();
                ViewModel.isXTurn = !ViewModel.isXTurn;                
            }
        }
    }
   
}
