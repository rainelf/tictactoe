﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Constants
    {
        public static int MAX_NUMBER_OF_MOVES = 9;
        public static int PVP = 0;
        public static int PVE = 1;
        public static char X_TAG = 'X';
        public static char O_TAG = '0';
        public static int PLAY_WITH_0 = 0;
        public static int PLAY_WITH_X = 1;
        public static string X_COORDINATE_PROPERTY = "XCoordinate";
        public static string Y_COORDINATE_PROPERTY = "YCoordinate";
    }
}
