﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TicTacToe
{
 
    public class ViewModel
    {
        public ICommand Mark { get; set; }

        public static bool isXTurn = true;

        public static int numberOfMoves = 0;

        public static int playMode = Constants.PVP;

        public static bool gameStarted = false;        

        public static int playerPlaysWith = Constants.PLAY_WITH_X;

        private List<Tuple<int, int>> xCells = new List<Tuple<int, int>>();
        private List<Tuple<int, int>> oCells = new List<Tuple<int, int>>();

        public static Rectangle Cell1, Cell2, Cell3, Cell4, Cell5, Cell6, Cell7, Cell8, Cell9;

        public ViewModel()
        {
            Mark = new Command(executeMethod, canExecuteMethod);
        }
        
        private bool canExecuteMethod(object parameter)
        {                     
            Rectangle cellClicked = (Rectangle)parameter;
            return Marked.GetIsMarked(cellClicked);
        }

        private void executeMethod(object parameter)
        {

            if (ViewModel.gameStarted == false)
            {
                MessageBox.Show("The game wasn't started");
                return;
            }
            numberOfMoves++;
            Rectangle cellClicked = (Rectangle)parameter;

            int x = XCoordinate.GetX(cellClicked);
            int y = YCoordinate.GetY(cellClicked);
            Tuple<int, int> currentTuple = new Tuple<int, int>(x, y);
                   
            ImageBrush imgBrush = new ImageBrush();
            if (isXTurn)
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"images/x.png", UriKind.Relative));
                ((Rectangle)parameter).Fill = imgBrush;
                xCells.Add(currentTuple);
                if(xCells.Count >= 3)
                {
                    if(checkWinner(xCells))
                    {
                        declareWinner(Constants.X_TAG.ToString());
                    }
                }                
            }
            else
            {
                imgBrush.ImageSource = new BitmapImage(new Uri(@"images/o.png", UriKind.Relative));
                ((Rectangle)parameter).Fill = imgBrush;
                oCells.Add(currentTuple);
                if (oCells.Count >= 3)
                {
                    if (checkWinner(oCells))
                    {
                        declareWinner(Constants.O_TAG.ToString());
                    }
                }
                
            }
           
            isXTurn = !isXTurn;
            Marked.SetIsMarked(cellClicked, true);

            if (numberOfMoves == Constants.MAX_NUMBER_OF_MOVES)
            {
                declareTie();
            }

            if (isComputerTurn())
            {
                computerMarksCell();
            }
        }

        private bool checkWinner(List<Tuple<int, int>> cells)
        {
            return scanLines(cells) || scanColumns(cells) || scanDiagonals(cells);
        }

        private bool scanLines(List<Tuple<int, int>> cells)
        {
            for (int i = 1; i <= 3; i++)
            {
                int matches = 0;
                foreach (Tuple<int, int> elem in cells)
                {
                    if (elem.Item1 == i)
                    {
                        matches++;
                    }
                    if (matches == 3)
                    {
                        return true;
                    }
                }                
            }
            return false;
        }

        private bool scanColumns(List<Tuple<int, int>> cells)
        {
            for (int i = 1; i <= 3; i++)
            {
                int matches = 0;
                foreach (Tuple<int, int> elem in cells)
                {
                    if (elem.Item2 == i)
                    {
                        matches++;
                    }
                    if (matches == 3)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool scanDiagonals(List<Tuple<int, int>> cells)
        {
            int matchesPrincipalDiag = 0, matchesSecondaryDiagonal = 0;
            foreach (Tuple<int, int> elem in cells)
            {
                if (elem.Item1 == elem.Item2)
                {
                    matchesPrincipalDiag++;
                }                

                if ((elem.Item1 + elem.Item2) == 4)
                {
                    matchesSecondaryDiagonal++;
                }

                if (matchesPrincipalDiag == 3 || matchesSecondaryDiagonal == 3)
                {
                    return true;
                }
            }

            return false;
        }

        private void declareWinner(String player)
        {
            MessageBox.Show(player + " wins!");
            resetApp();
        }

        private void declareTie()
        {
            MessageBox.Show("Nobody wins!");
            resetApp();

        }

        private void resetApp()
        {
            Process.Start(Application.ResourceAssembly.Location);                    
            Application.Current.Shutdown();
        }

        public bool isPlayerTurn()
        {
            if ((isXTurn && playerPlaysWith == Constants.PLAY_WITH_X) || (!isXTurn && playerPlaysWith == Constants.PLAY_WITH_0))
            {
                return true;
            }
            return false;
        }

        public bool isComputerTurn()
        {
            return !isPlayerTurn();
        }

        private Tuple<int, int> pickFreeCell ()
        {
            List<Tuple<int, int>> freeCells = new List<Tuple<int, int>>();
           
            for  (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    Tuple<int, int> tp = new Tuple<int, int>(i, j);
                    if (!xCells.Contains(tp) && !oCells.Contains(tp))
                    {
                        freeCells.Add(tp);
                    }
                }               
            }

            Random rnd = new Random();
            int randomIndex = rnd.Next(1, freeCells.Count);
            return freeCells[randomIndex];
        }

        public void computerMarksCell()
        {
            Tuple<int, int> chosenCell = this.pickFreeCell();

            List<Rectangle> allGraphicalCells = new List<Rectangle>();
            allGraphicalCells.Add(Cell1);
            allGraphicalCells.Add(Cell2);
            allGraphicalCells.Add(Cell3);
            allGraphicalCells.Add(Cell4);
            allGraphicalCells.Add(Cell5);
            allGraphicalCells.Add(Cell6);
            allGraphicalCells.Add(Cell7);
            allGraphicalCells.Add(Cell8);
            allGraphicalCells.Add(Cell9);

            foreach (Rectangle cell in allGraphicalCells)
            {
                if (checkIfRectangleCellWasPicked(cell, chosenCell))
                {
                    if (this.Mark.CanExecute(cell))
                    {
                        this.Mark.Execute(cell);
                    }
                    break;
                }
            }
           
            
        }

        private bool checkIfRectangleCellWasPicked(Rectangle cell, Tuple<int, int> chosenCell)
        {
            LocalValueEnumerator localValues = cell.GetLocalValueEnumerator();
            int x = -1, y = -1;
            while (localValues.MoveNext())
            {
                LocalValueEntry entry = localValues.Current;
                if (entry.Property.Name == Constants.X_COORDINATE_PROPERTY)
                {
                    x = (int) entry.Value;
                }
                if (entry.Property.Name == Constants.Y_COORDINATE_PROPERTY)
                {
                    y = (int)entry.Value;
                }
            }

            return (x == chosenCell.Item1 && y == chosenCell.Item2);
        }

    }
}
