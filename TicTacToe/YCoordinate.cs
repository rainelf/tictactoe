﻿using System;
using System.Windows;
using System.Windows.Shapes;

namespace TicTacToe
{
    class YCoordinate
    {
        public static readonly DependencyProperty YCoordinateProperty = DependencyProperty.RegisterAttached(
          "YCoordinate", typeof(int), typeof(YCoordinate), new PropertyMetadata(0));


        public static void SetY(Rectangle target, int value)
        {
            target.SetValue(YCoordinateProperty, value);
        }

        public static int GetY(Rectangle target)
        {
            return (int)target.GetValue(YCoordinateProperty);
        }
    }
}
