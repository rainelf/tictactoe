﻿using System;
using System.Windows.Input;
using System.Windows.Shapes;

namespace TicTacToe
{
    public class Command : ICommand
    {
        #pragma warning disable 67
        public event EventHandler CanExecuteChanged;

        Action<object> executeMethod;
        Func<object, bool> canExecuteMethod;

        public Command(Action<object> executedMethod, Func<object, bool> canExecuteMethod)
        {
            this.executeMethod = executedMethod;
            this.canExecuteMethod = canExecuteMethod;        
        }

        public bool CanExecute(object parameter)
        {
            Rectangle cellClicked = (Rectangle)parameter;
            return !Marked.GetIsMarked(cellClicked);
            
        }

        public void Execute(object parameter)
        {
            executeMethod(parameter);                                
        }
    }
}
