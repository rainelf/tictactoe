﻿using System;
using System.Windows;
using System.Windows.Shapes;

namespace TicTacToe
{
    class XCoordinate
    {
        public static readonly DependencyProperty XCoordinateProperty = DependencyProperty.RegisterAttached(
           "XCoordinate", typeof(int), typeof(XCoordinate), new PropertyMetadata(0));
    
        public static void SetX(Rectangle target, int value)
        {
            target.SetValue(XCoordinateProperty,value);
        }

        public static int GetX(Rectangle target)
        {
            return (int)target.GetValue(XCoordinateProperty);
        }
    }
}

